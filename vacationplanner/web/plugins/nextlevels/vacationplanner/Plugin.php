<?php namespace Nextlevels\Vacationplanner;

use AjaxException;
use App;
use System\Classes\PluginBase;

/**
 * Class Plugin
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class Plugin extends PluginBase
{

    /**
     * On boot
     *
     * @return array|void
     */
    public function boot()
    {
        App::error(function (AjaxException $exception) {
            return $exception->getContents();
        });
    }
}
