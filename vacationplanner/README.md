# Vacationplanner #

## Installation ##

* Clone the project: git clone -b develop `https://bitbucket.org/nextlevels/##REPO_URL##`
* Install composer packages with: `composer install`
* Install npm packages with: `npm install`
* Start your environment (CHOOSE ONE NOT BOTH!)
    * Docker with follow command: `docker-compose up -d --build apache`
    * OR Vagrant with follow command: `vagrant up`
* Import database dump (If no database dump in data folder exists, create dump from live server and put it to data folder and push it to the repo!)
* Add follow host in your host file, if you choose docker. Vagrant automatically update the host file
* HAPPY PROGRAMMING!

## Development tools ##

### How to use laravel mix/webpack ###
[Documentation](https://laravel-mix.com/docs/)
* First install npm packages with `npm install`
* Run webpack use follow command `npm run dev`
* To start webpack watcher use follow command `npm run watch`
* To compile assets for production system use follow command `npm run production`

### How to use docker ###
* Start docker container: `docker-compose up -d --build ##YOUR CONTAINER##`
* Stop all docker container in directory: `docker-compose down`
* Connect to a workspace container: `docker-compose exec php-fpm sh`

### How to use vagrant ###
* Start vagrantbox: `vagrant up`
* Update vagrantbox if vagrant config changed: `vagrant up --provision`
* Stop vagrantbox: `vagrant halt`
* Connect to the vagrant box: `vagrant ssh`


### How to use deployer ###
* First change credentials in `deploy.php`
* To deploy your project run follow command in root directory: `bin/dep deploy`

### How to use php-cs-fixer ###
* Run follow command: `bin/php-cs-fixer`

## Host ##

* WIN: `C:\Windows\System32\drivers\etc`
* MacOS/Linux: `/etc/hosts`
````
127.0.0.1         vacationplanner.local
````

## Credentials ##

### Database (Docker) ###
* Host: vacationplanner_mysql_1
* Database: vacationplanner
* Username: vacationplanner_u
* Password: vacationplanner_p

### Database (Vagrant) ###
* Host: localhost
* Database: vacationplanner
* Username: homestead
* Password: secret

## URL's ##

Website
````
https://vacationplanner.local/
````

Backend
````
https://vacationplanner.local/####INSERT BACKEND PAGE HERE####
````

Mailhog
````
https://vacationplanner.local:8025/
````

## Troubleshooting ##

### Shopware ###
* Update host/baseURL in DATABASE in `sCore_shops` table

### Vagrant ###
* It appears your machine doesn't support NFS, or there is not an adapter to enable NFS on this machine for Vagrant.
* SOLUTION: sudo apt-get install nfs-kernel-server nfs-common