<?php namespace Deployer;

require 'recipes/nextlevels.php';

/**
 * Configuration
 */
set('ssh_multiplexing', true);
set('repository', 'https://nextlevelsserver:levelsnext99@bitbucket.org/nextlevels/##REPO NAME##');
set('default_stage', 'testing');

add('copy_dirs', ['web/vendor']);

/**
 * Servers
 */
server('testing', '##SERVER_HOST##')
    ->stage('testing')
    ->user('##SERVER_USER##')
    ->password('##SERVER_USER_PASSWORD##')
    ->set('deploy_path', '/html/develop')
    ->set('branch', 'develop')
    ->pty(true);

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:copy_dirs',
    'deploy:vendors',
    'deploy:writable',
    'october:env',
    'october:migrate',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);

after('deploy', 'success');
after('deploy:failed', 'deploy:unlock');
